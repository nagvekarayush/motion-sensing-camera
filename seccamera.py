import cv2
import winsound
camera = cv2.VideoCapture(0)
  
while(True):
    ret, frame1 = camera.read()
    ret, frame2 = camera.read()
    diffrence = cv2.absdiff(frame1,frame2)
    greyimage = cv2.cvtColor(diffrence,cv2.COLOR_RGB2GRAY)
    blurimage = cv2.GaussianBlur(greyimage,(5,5),0)
    _, threshold = cv2.threshold(blurimage,20,255 ,cv2.THRESH_BINARY)
    dilatedimage = cv2.dilate(threshold,None,iterations=4)
    contours, x = cv2.findContours(dilatedimage,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    #cv2.drawContours(frame1,contours,-1,(0, 255,0), 3)


                
    for y in contours:
        if cv2.contourArea(y) < 5000:
            continue
        aa,ab,w,h = cv2.boundingRect(y)
        cv2.rectangle(frame1,(aa,ab),(aa+w,ab+h),(0,255,0), 3)
        winsound.Beep(500,500)
    cv2.imshow('frame', frame1 )
    if cv2.waitKey(1) & 0xFF == ord('a'): 
        break
camera.release()
cv2.destroyAllWindows() 

print("Done!")
 